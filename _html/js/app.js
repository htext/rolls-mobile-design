var $ = jQuery;
$.fn.exists = function () {
    return this.length !== 0;
}
/** 
 * 
 * Start script 
 * 
 * */
$(function() {
    easy_modal_init();

    $('.wan-spinner.prod-spinner').each(function() {
        var spinner = this;
        $(spinner).WanSpinner({
            inputWidth: 50,
            maxValue: $(spinner).data("max"),
            minValue: 0,
            plusClick: function(el, val) {
                plusProdItem(this, spinner, val) 
            },
            minusClick: function(el, val) {
                minusProdItem(this, spinner, val) 
            }
        });
    });

    $('.wan-spinner.cart-spinner').each(function() {
        var spinner = this;
        $(spinner).WanSpinner({
            inputWidth: 50,
            maxValue: $(spinner).data("max"),
            minValue: 0,
            plusClick: function(el, val) {
                plusProdItem(this, spinner, val) 
            },
            minusClick: function(el, val) {
                minusProdItem(this, spinner, val, true) 
            }
        });
    });
    
    $('.btn.prod-btn-cart').click(function(e) {
        e.preventDefault();
        $('.btn.prod-btn-cart[data-variant*='+$(this).data("variant")+']').addClass('d-none');
        $('.wan-spinner.prod-btn-cart[data-variant*='+$(this).data("variant")+']').removeClass('d-none');
        $('.wan-spinner.prod-btn-cart[data-variant*='+$(this).data("variant")+'] input').val(1);
        sendProdToCart();
    });

    tabby.init();

    $('.btn-user').click(function (e) {
        if ($(this).hasClass('authorized'))
        {
            e.preventDefault();
            $(this).siblings('.prod-user').addClass('visible');
        }
    });

    $('.prod-user-close').click(function (e) {
        $(this).parent().parent().parent().removeClass('visible');
    });

    registerInvertClick('prod-user-meta', function() { $('.prod-user').removeClass('visible'); });

    $('.main-carousel.home-carousel').flickity({
        wrapAround: true
    });

    $('.main-carousel.cart-additionals').flickity({
        wrapAround: true,
        pageDots: false,
        groupCells: true,
        freeScroll: true
    });

    $('.main-carousel.cart-drinks').flickity({
        wrapAround: true,
        pageDots: false,
        groupCells: true,
        freeScroll: true
    });

    $('.cart-btn-del').click(function(e) {
        e.preventDefault();
        $(this).parent().parent().remove();
        sendProdToCart();
    });

    $('.btn-promo').click(function (e) {
        e.preventDefault();
        $(this).siblings('.input-promo').removeClass('d-none');
        $(this).addClass('d-none');
    });

    $('.wan-spinner.order-spinner').each(function() {
        var spinner = this;
        $(spinner).WanSpinner({
            inputWidth: 50,
            maxValue: $(spinner).data("max"),
            minValue: 0,
            plusClick: function(el, val) {
                plusProdItem(this, spinner, val) 
            },
            minusClick: function(el, val) {
                minusProdItem(this, spinner, val, true) 
            }
        });
    });
});
/** 
 * 
 * /Start script/ 
 * 
 * */
function registerInvertClick(clazz, fun) {
    $('body').click(function (e) {
		if (!$(e.target).hasClass(clazz)) fun();
    });
}

function sendProdToCart() {
    //TODO
}

function plusProdItem(spinner, element, val) {
    sendProdToCart();
}

function minusProdItem(spinner, element, val, incart) {
    if ( val < 1) {
        if (incart == undefined) 
        {
            $('.btn.prod-btn-cart[data-variant*='+$(element).data("variant")+']').removeClass('d-none');
            $('.wan-spinner.prod-btn-cart[data-variant*='+$(element).data("variant")+']').addClass('d-none');
        }
        $(element).find('input').val(1);   
    }
    sendProdToCart();
}