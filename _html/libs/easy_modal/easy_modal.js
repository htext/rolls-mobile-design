/**
 * Создаем модалку с классов easy_modal.
 * Создаем уникальный ID
 * В нем кнопку easy_modal_close.
 * В ссылке указываем href=id модалки
 * Не забываем установить overlay
 */
function easy_modal_init() {
    var overlay = $('#easy_modal_overlay');
    var open_modal = $('.easy_modal_open');
    var close = $('.easy_modal_close, #easy_modal_overlay');
    var modal = $('.easy_modal');
    open_modal.click(function (event) {
        event.preventDefault();
        var div = $(this).attr('href');
        overlay.fadeIn(400,
            function () {
                $(div).css('display', 'block').animate({ opacity: 1 }, 200);
            });
    });
    close.click(function () {
        modal.animate({opacity: 0}, 200, 
            function () {
                $(this).css('display', 'none');
                overlay.fadeOut(400);
            });
    });
}