var $ = jQuery;
$.fn.exists = function () {
    return this.length !== 0;
}
/** 
 * 
 * Start script 
 * 
 * */
var progressbar;

$(function() {
    easy_modal_init();

    $('.wan-spinner.prod-spinner').each(function() {
        var spinner = this;
        $(spinner).WanSpinner({
            inputWidth: 50,
            maxValue: 999,
            minValue: 0,
            minusClick: function(el, val) {
                if ( val < 1) {
                    $('.btn.prod-btn-cart[data-variant*='+$(el).data("variant")+']').removeClass('d-none');
                    $('.wan-spinner.prod-btn-cart[data-variant*='+$(el).data("variant")+']').addClass('d-none');
                    $(el).find('input').val(1);
                }
            },
            valueChanged: function(el, val) {
                sendProdToCart($(el).data('variant'), val);
            }
        });
    });
    
    $('.btn.prod-btn-cart').click(function(e) {
        e.preventDefault();
        $('.btn.prod-btn-cart[data-variant*='+$(this).data("variant")+']').addClass('d-none');
        $('.wan-spinner.prod-btn-cart[data-variant*='+$(this).data("variant")+']').removeClass('d-none');
        $('.wan-spinner.prod-btn-cart[data-variant*='+$(this).data("variant")+'] input').val(1);
        sendProdToCart($(this).data("variant"), 1);
    });

    tabby.init();

    $('.btn-user').click(function (e) {
        if ($(this).hasClass('authorized'))
        {
            e.preventDefault();
            $(this).siblings('.prod-user').addClass('visible');
        }
    });

    $('.prod-user-close').click(function (e) {
        $(this).parent().parent().parent().removeClass('visible');
    });

    registerInvertClick('prod-user-meta', function() { $('.prod-user').removeClass('visible'); });

    $('.main-carousel.home-carousel').flickity({
        wrapAround: true
    });


    $('.btn-promo').click(function (e) {
        e.preventDefault();
        $(this).siblings('.input-promo').removeClass('d-none');
        $(this).addClass('d-none');
    });

    $('.wan-spinner.order-spinner').each(function() {
        var spinner = this;
        $(spinner).WanSpinner({
            inputWidth: 50,
            maxValue: 100,
            minValue: 1
        });
    });

    if ($('#promo-progbar').exists()) {
        progressbar = new ProgressBar.Line('#promo-progbar', {
            color: '#e94d26'
        });
    }
    cartInit();

    $('#dyn_cart').submit(function() {
        var address = $(this).find('input[name="address_fake"]').val();
        address += $(this).find('input.js-in-home').val() != '' ? ", д. " + $(this).find('input.js-in-home').val() : '';
        address += $(this).find('input.js-in-room').val() != '' ? ", кв. " + $(this).find('input.js-in-room').val() : '';
        address += $(this).find('input.js-in-pod').val() != '' ? ", п. " + $(this).find('input.js-in-pod').val() : '';
        address += $(this).find('input.js-in-et').val() != '' ? ", эт. " + $(this).find('input.js-in-et').val() : '';
        address += $(this).find('input.js-in-name').val() != '' ? ", Название: " + $(this).find('input.js-in-name').val() : '';
        $(this).find('input[name="address"]').val(address);

        var comment = $(this).find('textarea[name="comment_fake"]').val();
        var complects = "Комплектов: " + $(this).find('.js-input-complect').val();
        comment = complects + (comment != '' ? "; Сообщение: " + comment : '');
        $(this).find('input[name="comment"]').val(comment);
    });

    easydropdown.all();
    $('#register_phone_check').click(function(e) {
        var phone = $(this).parent().siblings('input[name="phone"]').val();
        window.sessionStorage.setItem('phone', phone);
        if (phone != '') {
            registerCheckPhone( $('#login_modal_sms'), phone );
        }
        else {
            e.preventDefault();
        }
    });
    $('#register_code_check').click(function(e) {
        var code = $(this).parent().siblings('input[name="code"]').val();
        if (code != '') {
            registerCheckCode($(this).parent().siblings('.requestInform.error'), code);
        }
        else {
            e.preventDefault();
        }
    });
    $('#timeout_sms').click(function(e) {
        if (timer_t > 0) e.preventDefault();
        else {
            $.ajax({
                url: 'ajax/phone_register.php',
                data: {mode: 'send_code', phone: window.sessionStorage.getItem('phone')},
                type: 'POST',
                dataType: 'json',
                success: function(data) {
                    if (data.success == 1)
                    {
                        console.log(data); //TODO REMOVE
                        setTimeoutSendCode(data.timeout);
                    }
                }
            });
        }
    });
    $('#login_link').click(function(e) {
        e.preventDefault();
        registerCheckAlreadyPhone();
    });
    $('#login_link_side').click(function(e) {
        e.preventDefault();
        registerCheckAlreadyPhone();
    });

    $(".js-scroll-to").click(function(e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $( $(this).attr('href') ).offset().top
        }, 1000);
    });
});

function cartInit() {
    $('.cart-btn-del').click(function(e) {
        e.preventDefault();
        sendProdToCart($(this).data("variant"));
        fullUpdateCart();
    });

    $('.wan-spinner.cart-spinner').each(function() {
        var spinner = this;
        $(spinner).WanSpinner({
            inputWidth: 50,
            maxValue: 999,
            minValue: 1,
            valueChanged: function(el, val) {
                sendProdToCart($(el).data('variant'), val);
            }
        });
    });

    $('.btn.prod-btn-cart-adds').click(function(e) {
        e.preventDefault();
        sendProdToCart($(this).data("variant"), 1);
        fullUpdateCart();
    });

    $('.main-carousel.cart-additionals').flickity({
        pageDots: false,
        groupCells: 2,
        freeScroll: true
    });

    $('.main-carousel.cart-drinks').flickity({
        pageDots: false,
        groupCells: 2,
        freeScroll: true
    });

    $('.btn-cart-next').click(function(e) {
        if ($(this).data('step') == 1) {
            e.preventDefault();
            $(this).data('step', 2)
            $(this).val('Заказать');

            $('.btn-cart-back').text('Назад к корзине');
            $('.btn-cart-back').data('step', 2);

            $('.js-cart-step-1').addClass('d-none');
            $('.js-cart-step-2').removeClass('d-none');
            window.scrollTo(0, 0);

            checkDelivPrice();
        } else {
            //e.preventDefault();
        }
    });

    $('.btn-cart-back').click(function(e) {
        if ($(this).data('step') == 2) {
            e.preventDefault();
            $(this).data('step', 1)
            $(this).text('Вернуться в меню');

            $('.btn-cart-next').data('step', 1)
            $('.btn-cart-next').val('Оформить заказ');

            $('.js-cart-step-2').addClass('d-none');
            $('.js-cart-step-1').removeClass('d-none');
            window.scrollTo(0, 0);

            var total = $('#total_price').data('price');
            $('#total_price').text((""+total).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
        }
    });

    $('.js-deliv-input').change(function() {
        checkDelivPrice();
    });

    $('input[name="phone"]').mask("+7 999 999-99-99");

    updatePromoPrices();
    easydropdown.all();
}

function updatePromoPrices() {
    if ($('#promo-progbar').exists()) { 
        var total_price = Number( $('#total_price').data('price-without-auto-coupon') );
        
        var min_price = 0;
        var result_disc = 0;
        $('.promo-min-price').each(function (e) {
            var price = $(this).text();
            if (price > min_price) min_price = $(this).text();
        
            var $need = $(this).parent().parent().find('.promo-need > span');
            var disc = Number( $(this).parent().parent().find('.promo-disc > span').text() );
        
            if (total_price >= price)
            {
                $need.text('0');
                result_disc = result_disc < disc ? disc : result_disc;
                $(this).parent().parent().addClass('promo-color');
            }
            else
            {
                $(this).parent().parent().removeClass('promo-color');
                $need.text(price - total_price);
            }
        });
        var progress = total_price / min_price;
        progressbar.animate(progress > 1 ? 1 : progress);

        total_price = total_price * (100 - result_disc) / 100;
        $('#total_price').text((""+total_price).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ')); 
        $('#total_price').data('price', total_price); 
    }
}

function registerInvertClick(clazz, fun) {
    $('body').click(function (e) {
		if (!$(e.target).hasClass(clazz)) fun();
    });
}

function checkDelivPrice() {
    var deliv = $('.js-deliv-input:checked');
    var min = deliv.siblings('label').find('.js-deliv-min-from').data('min');
    var price = deliv.siblings('label').find('.js-deliv-price').data('price');
    var total = $('#total_price').data('price');
    if (price > 0 && total <= min) {
        var tprice = total + price;
        $('#total_price').text((""+tprice).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
    } else {
        $('#total_price').text((""+total).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
    }
}

function sendProdToCart(v_id, cnt) {
    if (cnt == undefined || cnt == 0)
    {
        $.ajax({
            url: 'cart/remove/'+v_id,
            type: 'GET',
            success: function() {
                updateCart();
            }
        });
    }
    else 
    {
        var request = new FormData();   
        request.append('amounts['+v_id+']',cnt);
        $.ajax({
            url: 'cart',
            data: request,
            type: 'POST',
            processData : false,
            contentType: false,
            success: function() {
                updateCart();
            }
        });
    }
}

function updateCart() {
    $.ajax({
		url: "ajax/cart.php",
		success: function(data){
			$('#cart_informer').html(data);
		}
    });
    if ($('#total_price').exists())
    {
        var total = 0;
        $('.price-text.prod-price > span').each(function(index, element) {
            var price = $(element).data('value');
            var amount = $(element).parent().parent().parent().find('.js-input-amount').val();
            total += price * amount;
        });
        var disc_value = $('#discount_value').data('value');
        var disc_type = $('#discount_value').data('type');
        var disc_min = $('#discount_value').data('min');
        var disc;
        if (total > disc_min) {
            if (disc_type == 'percentage') 
            {
                disc = total * (disc_value / 100)
                total *= (100 - disc_value) / 100;
                
            }
            else if (disc_type == 'absolute')
            {
                disc = disc_value;
                total -= disc_value;
            }
            $('#discount_value').text((""+disc).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
        } else { 
            $('#discount_value').parent().parent().parent().parent().removeClass('d-flex');
            $('#discount_value').parent().parent().parent().parent().addClass('d-none');
            
            $('#discount_value').parent().parent().parent().parent().siblings('.cart-active-coupon').removeClass('d-none');
            $('#discount_value').parent().parent().parent().parent().siblings('.cart-active-coupon').addClass('d-flex');
        }
        $('#total_price').text((""+total).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
        $('#total_price').data('price', total); 
        $('#total_price').data('price-without-auto-coupon', total)
        updatePromoPrices();
    }
}

function fullUpdateCart() {
    setTimeout(() => {
        $.ajax({
            url: 'cart?dyn=1',
            data: {},
            type: 'POST',
            success: function(data) {
                var static = $('#dyn_cart > .js-static-btns');
                $('#dyn_cart').html(data);
                static.appendTo($('#dyn_cart'));
                cartInit();
            }
        });
    }, 30);
}

function registerCheckPhone(modal, phone, mode) {
    if (mode == undefined) mode = 'send_code';
    $.ajax({
        url: 'ajax/phone_register.php',
        data: {mode: mode, phone: phone},
        type: 'POST',
        dataType: 'json',
        success: function(data) {
            if (data.success == 1)
            {
                easy_showModal(modal, $('#easy_modal_overlay'));
                $( modal ).find('.requestInform.error').addClass('d-none');
                setTimeoutSendCode(data.timeout);
            }
            else
            {
                var feedback = $(modal).find('.requestInform.error');
                feedback.text(data.reason);
                feedback.removeClass('d-none');
            }
            console.log(data); //TODO REMOVE
        }
    });
}

var timer_id;
var timer_t;
function setTimeoutSendCode(timeout) {
    clearInterval(timer_id);
    timer_t = timeout;
    timer_id = setInterval(() => {
        var timer = $('#timeout_sms');
        timer.removeClass('enabled');
        timer.text('Отправить снова: '+timer_t + 'с.');
        if (--timer_t <= 0) {
            clearInterval(timer_id);
            timer.text('Отправить снова');
            timer.addClass('enabled');
        }
    }, 1000);
}

function registerCheckCode(feedback, code) {
    $.ajax({
        url: 'ajax/phone_register.php',
        data: {mode: 'check_code', code: code, phone: window.sessionStorage.getItem('phone')},
        type: 'POST',
        dataType: 'json',
        success: function(data) {
            if (data.success == 1)
            {
                document.location.replace('/user');
            }
            else 
            {
                feedback.text(data.reason);
                feedback.removeClass('d-none');
            }
        }
    });
}

function registerCheckAlreadyPhone() {
    $.ajax({
        url: 'ajax/phone_register.php',
        data: {mode: 'get_timeout', phone: window.sessionStorage.getItem('phone')},
        type: 'POST',
        dataType: 'json',
        success: function(data) {
            if (data.success == 1)
            {
                registerCheckPhone( $('#login_modal_sms'), window.sessionStorage.getItem('phone'), 'get_timeout' );
            }
            else
            {
                easy_showModal($('#login_modal'), $('#easy_modal_overlay'));
            }
        }
    });
}