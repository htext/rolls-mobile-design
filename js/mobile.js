$(document).on( 'click','.mobile-menu__open', function(){
    $('.sidenav').fadeIn(500);
    $('#sidenav').addClass('d-flex');
});
$(document).on('click', '.sidenav_overlay', function () {
    $('.sidenav').fadeOut(500, function () {
        $('#sidenav').removeClass('d-flex');

    });
});
$('.main-carousel').flickity({
    draggable: true,
    wrapAround: true,
    // prevNextButtons: false,
    pageDots: false
});
