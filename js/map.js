ymaps.ready(function () {
	// Создание экземпляра карты и его привязка к созданному контейнеру
	// координаты задаются непосредственно на странице в переменную destinations
	if (typeof destinations !== 'undefined') {
		var myMap = new ymaps.Map("map_container", {
			center: destinations['center'],
			zoom: 17,
			controls: []
		});

		myMap.controls
			.add('zoomControl', {
				position: {
					top: 100,
					right: 10,
				}
			})
			.add("fullscreenControl");

		myMap.behaviors.disable(['scrollZoom']);

		var plc1 = new ymaps.Placemark(destinations['point'], {}, {
			iconLayout: 'default#image',
			iconImageHref: '//' + location.host + '/design/vkusroll/img/point.png',
			iconImageSize: [51, 70],
			iconImageOffset: [-25, -70]
		});

		myMap.geoObjects.add(plc1);
	}
});