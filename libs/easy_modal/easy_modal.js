/**
 * Создаем модалку с классов easy_modal.
 * Создаем уникальный ID
 * В нем кнопку easy_modal_close.
 * В ссылке указываем href=id модалки
 * Не забываем установить overlay
 */
function easy_modal_init() {
    var overlay = $('#easy_modal_overlay');
    var open_modal = $('.easy_modal_open');
    var close = $('.easy_modal_close, #easy_modal_overlay');
    var modal = $('.easy_modal');
    open_modal.click(function (event) {
        event.preventDefault();
        easy_showModal( $( $(this).attr('href') ), overlay);
    });
    close.click(function () {
        modal.animate({opacity: 0}, 200, 
            function () {
                $(this).css('display', 'none');
                overlay.fadeOut(400);
            });
    });
}

function easy_showModal(modal, overlay) {
        overlay.fadeIn(400,
            function () {
                $(modal).css('display', 'block').animate({ opacity: 1 }, 200);
            });
}

function easy_closeModals(overlay) {
    var modal = $('.easy_modal');
    modal.animate({opacity: 0}, 200, 
        function () {
            $(this).css('display', 'none');
            overlay.fadeOut(400);
        });
}