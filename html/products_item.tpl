{$v = $product->variants|@first}
{$option = ''}
{$option_name = ''}
{$prod_dual = 0}
{* {$product->options|os_log} *} {* //TODO REMOVE *}
{foreach $product->options as $opt}
	{if $opt->feature_id == 153}
		{$prod_dual = $opt->value}
	{else}
		{$option_name = $opt->name}
		{$option = $opt->value}
	{/if}
{/foreach}
{$amount = 0}
{foreach $cart->purchases as $purchase}
	{if $purchase->variant->id == $v->id}
		{$amount = $purchase->amount}
	{/if}
{/foreach}
{if $type == 'drink'} {$prod_dual = 1} {/if}
<div class="prod-card {if $prod_dual}prod-card__dual col-6 col-lg-12 {if $type == 'drink'}col-xl-6{else}col-xl-8{/if}{else}col-6 col-lg-6 col-xl-4{/if} d-flex justify-content-center">
	<article class="prod bd mg-btm {if $prod_dual}prod-dual {if $type == 'drink'}prod-md{/if} d-flex justify-content-between{elseif $type == 'adds'}prod-sm{/if}">
		{if $prod_dual}<div>{/if}
		<div class="prod-images p-relative">
			<div class="prod-labels">
				{if $product->hit}<span class="prod-label prod-label-hit">ХИТ</span>{/if}
				{if $product->new}<span class="prod-label prod-label-new">НОВИНКА</span>{/if}
			</div>
			<div class="prod-img d-flex">
				<img class="mg-auto" src="{$product->image->filename|resize:200:200}" alt="{$product->name|escape}">
			</div>
			{if $type == 'kit' || $type == 'roll'}
			<div class="prod-adds">
				{if $type == 'kit'}
				<div class="d-inline-block"><img src="design/{$settings->theme|escape}/img/i-wasabi.png"></div>
				<div class="d-inline-block"><img src="design/{$settings->theme|escape}/img/i-sauce.png"></div>
				<div class="d-inline-block"><img src="design/{$settings->theme|escape}/img/i-ginger.png"></div>
				{/if}
				{if $spicy}<div class="d-inline-block fl-right"><img src="design/{$settings->theme|escape}/img/pepper.png"></div>{/if}
			</div>
			{/if}
		</div>
		{if $prod_dual}</div><div>{/if}
		<h5>{$product->name|escape}</h5>
		<div class="g-text">
			{$option_name}: {$option}
		</div>
		<div class="prod-main">
			<div class="g-text">{$product->body}</div>
		</div>
		<span class="price-text prod-price">{$v->price|convert} {$currency->sign|escape}</span>
		<a href="cart?variant={$v->id}" class="btn prod-btn-cart {if $amount > 0}d-none{/if}" variant-id='{$v->id}' data-amount="1"  data-variant="{$v->id}">В корзину</a>
		<div class="wan-spinner prod-spinner prod-btn-cart {if $amount == 0}d-none{/if}" data-variant="{$v->id}">
			<a href="javascript:void(0)" class="minus btn btn-white">-</a>
			<input type="text" value="{$amount}">
			<a href="javascript:void(0)" class="plus btn btn-white">+</a>
		</div>
		{if $prod_dual}</div>{/if}
	</article>
</div>