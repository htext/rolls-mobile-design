{* Шаблон текстовой страницы *}

{* Канонический адрес страницы *}
{$canonical="/{$page->url}" scope=parent}

<section class="container d-flex justify-content-center">
    <arcitcle class="inner-container page">
        <!-- Заголовок страницы -->
        <h1 data-page="{$page->id}">{$page->header|escape}</h1>
        <!-- Тело страницы -->
        {$page->body}
    </arcitcle>
</section>