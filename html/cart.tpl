{* Шаблон корзины *}

{$meta_title = "Корзина" scope=parent}

{if $smarty.get.dyn && $smarty.server.HTTP_X_REQUESTED_WITH|strtolower == 'xmlhttprequest'}
	{$wrapper = 'index_dyn.tpl' scope=parent}
	{$dyn = 1}
{/if}

{* STATIC >> *} {if !$dyn} {* << STATIC *}

<form method="post" name="cart" class="container d-flex justify-content-center flex-column" id="dyn_cart">
{* /STATIC << *} {/if} {* >> /STATIC *}
	<article class="inner-container js-cart-step-1">
		<h1>
			{if $cart->purchases}{*В корзине {$cart->total_products} {$cart->total_products|plural:'товар':'товаров':'товара'}*}
				Ваш заказ
			<span class="fl-right steps g-text">Шаг 1 из 2</span>
			{else}Корзина пуста{/if}
		</h1>
		{if $cart->purchases}
			{foreach $cart->purchases as $purchase}
				{$product = $purchase->product}
				{$image = $product->images|first}
				{$option_name = ''}
				{$options = $api->features->get_product_options($product->id)}
				{$option = ''}
				{foreach $options as $opt}
					{if $opt->feature_id == 153}
						{$prod_dual = $opt->value}
					{else}
						{$option_name = $opt->name}
						{$option = $opt->value}
					{/if}
				{/foreach}
				{* {$purchase|os_log}  *}{* //TODO REMOVE *}
				<article class="prod bd mg-btm prod-dual prod-incart d-flex justify-content-between w-100">
					<div class="prod-incart-img">
						<div class="prod-images p-relative">
							<div class="prod-labels">
								{if $product->hit}<span class="prod-label prod-label-hit">ХИТ</span>{/if}
								{if $product->new}<span class="prod-label prod-label-new">НОВИНКА</span>{/if}
							</div>
							<a href="#" class="prod-img d-flex">
								<img class="mg-auto" src="{$image->filename|resize:200:200}">
							</a>
							<div class="prod-adds">
								{if $product->category->id == 24}
								<div class="d-inline-block fl-right"><img src="design/{$settings->theme|escape}/img/pepper.png"></div>
								{/if}
							</div>
						</div>
					</div>
					<div class="prod-incart-desc d-flex">
						<div class="mg-auto">
							<h5>{$product->name|escape}</h5>
							<div class="g-text">
								{$option_name}: {$option}
								{if $product->category->id == 20}
								<div class="prod-adds">
									<div class="d-inline-block"><img src="design/{$settings->theme|escape}/img/i-wasabi.png"></div>
									<div class="d-inline-block"><img src="design/{$settings->theme|escape}/img/i-sauce.png"></div>
									<div class="d-inline-block"><img src="design/{$settings->theme|escape}/img/i-ginger.png"></div>
								</div>
								{/if}
							</div>
						</div>
					</div>
					<div class="d-flex">
						<div class="wan-spinner cart-spinner prod-btn-cart mg-auto" data-variant="{$purchase->variant->id}">
							<a href="javascript:void(0)" class="minus btn btn-white">-</a>
							<input class="js-input-amount" name="amounts[{$purchase->variant->id}]" type="text" value="{$purchase->amount}">
							<a href="javascript:void(0)" class="plus btn btn-white">+</a>
						</div>
					</div>
					<div class="prod-incart-price">
						<span class="price-text prod-price"><span data-value="{$purchase->variant->price}">{$purchase->variant->price|convert}</span> {$currency->sign}</span>
					</div>
					<div class="prod-incart-del">
						<a href="#" class="bd cart-btn-del" data-variant="{$purchase->variant->id}">
							<img src="design/{$settings->theme|escape}/img/i-del.svg">
						</a>
					</div>
				</article>
			{/foreach}
			{$adds = $api->products->get_fit_products(['visible' => 1, 'category_id' => 23])}
			{$adds_flag = 0}
			{foreach $adds as $product}
				{$v = $product->variants|@first}
				{foreach $cart->purchases as $purchase}
					{if $v->id == $purchase->variant->id} {$adds_flag = $adds_flag + 1} {break} {/if}
				{/foreach}
			{/foreach}
			{if $adds|count > 0 && $adds_flag != $adds|count}
				<h3>Дополнительно</h3>
				<div class="main-carousel cart-additionals">
					{foreach $adds as $product}
						{$v = $product->variants|@first}
						{$flag = 1}
						{foreach $cart->purchases as $purchase}
							{if $v->id == $purchase->variant->id} {$flag = 0} {break} {/if}
						{/foreach}
						{if $flag == 1}
							{foreach $product->options as $opt}
								{if $opt->feature_id == 152}
									{$option = $opt->value}
								{elseif $opt->feature_id == 154}
									{$option = $opt->value}
								{/if}
							{/foreach}
							<div class="carousel-cell">
								<article class="prod bd mg-btm prod-dual prod-cart-adds prod-md d-flex justify-content-between">
									<div>
										<div class="prod-images p-relative">
											<div class="prod-labels">
												{if $product->hit}<span class="prod-label prod-label-hit">HIT</span>{/if}
												{if $product->new}<span class="prod-label prod-label-new">NEW</span>{/if}
											</div>
											<a href="#" class="prod-img d-flex">
												<img class="mg-auto" src="{$product->image->filename|resize:200:200}" alt="{$product->name|escape}">
											</a>
										</div>
									</div>
									<div>
										<h5>{$product->name|escape}</h5>
										<div class="g-text">{$option}</div>
										<div class="margin"></div>
										<span class="price-text prod-price">{$v->price|convert} {$currency->sign|escape}</span>
										<a href="#" class="btn prod-btn-cart-adds btn-white fl-right" data-variant="{$v->id}" variant-id='{$v->id}' data-amount="1">В корзину</a>
									</div>
								</article>
							</div>
						{/if}
					{/foreach}
				</div>
			{/if}
			{$drinks = $api->products->get_fit_products(['visible' => 1, 'category_id' => 22])}
			{$drinks_flag = 0}
			{foreach $drinks as $product}
				{$v = $product->variants|@first}
				{foreach $cart->purchases as $purchase}
					{if $v->id == $purchase->variant->id} {$drinks_flag = $drinks_flag + 1} {break} {/if}
				{/foreach}
			{/foreach}
			{if $drinks|count > 0 && $drinks_flag != $drinks|count}
				<h3>Напитки</h3>
				<div class="main-carousel cart-drinks">
					{foreach $drinks as $product}
						{$v = $product->variants|@first}
						{$flag = 1}
						{foreach $cart->purchases as $purchase}
							{if $v->id == $purchase->variant->id} {$flag = 0} {break} {/if}
						{/foreach}
						{if $flag == 1}
							{foreach $product->options as $opt}
								{if $opt->feature_id == 152}
									{$option = $opt->value}
								{elseif $opt->feature_id == 154}
									{$option = $opt->value}
								{/if}
							{/foreach}
							<div class="carousel-cell">
								<article class="prod bd mg-btm prod-dual prod-cart-adds prod-md d-flex justify-content-between">
									<div>
										<div class="prod-images p-relative">
											<div class="prod-labels">
												{if $product->hit}<span class="prod-label prod-label-hit">HIT</span>{/if}
												{if $product->new}<span class="prod-label prod-label-new">NEW</span>{/if}
											</div>
											<a href="#" class="prod-img d-flex">
												<img class="mg-auto" src="{$product->image->filename|resize:200:200}" alt="{$product->name|escape}">
											</a>
										</div>
									</div>
									<div>
										<h5>{$product->name|escape}</h5>
										<div class="g-text">{$option}</div>
										<div class="margin"></div>
										<span class="price-text prod-price">{$v->price|convert} {$currency->sign|escape}</span>
										<a href="#" class="btn prod-btn-cart-adds btn-white fl-right" data-variant="{$v->id}" variant-id='{$v->id}' data-amount="1">В корзину</a>
									</div>
								</article>
							</div>
						{/if}
					{/foreach}
				</div>
			{/if}
		{else}
		В корзине нет товаров
		{/if}
	</article>
	<arcitcle class="inner-container bd-btm pd-btm-20 d-none js-cart-step-2">
		<h1>Оформление заказа <span class="fl-right steps g-text">Шаг 2 из 2</span></h1>
		{if $error}
		<div class="requestInform error">
			{if $error == 'empty_name'}Введите имя{/if}
			{if $error == 'empty_email'}Введите email{/if}
			{if $error == 'captcha'}Капча введена неверно{/if}
		</div>
		{/if}
		<article class="prod bd w-100 prod-order-panel mg-btm-20">
			<h4>ДЕТАЛИ ЗАКАЗА</h4>
			<div class="d-inline-block w-270px">Ваше имя*</div>
			<div class="d-inline-block w-420px">
				<input type="text" name="name" class="input bd w-100 order-input" placeholder="Как к Вам обращаться?" data-format=".+" data-notice="Введите имя" value="{$user->name}">
			</div>
			<div class="d-inline-block w-270px">Номер телефона*</div>
			<div class="d-inline-block w-420px">
				<input type="text" name="phone" class="input bd w-100 order-input" placeholder="+7___-__-__" data-format=".+" data-notice="Введите телефон" value="{$user->phone}">
			</div>
			<div class="d-inline-block w-270px">Комплектов приборов</div>
			<div class="d-inline-block w-420px">
				<div class="wan-spinner order-spinner order-input" data-max="5" data-variant="88">
					<a href="javascript:void(0)" class="minus btn btn-white">-</a>
					<input type="text" class="js-input-complect" value="1">
					<a href="javascript:void(0)" class="plus btn btn-white">+</a>
				</div>
			</div>
			<div class="d-inline-block w-270px">Готовность</div>
			<div class="d-inline-block w-420px">
				<input type="radio" class="input bd order-input order-radio js-order-time-now" value="now" name="ready" id="ready_now" checked>
				<label for="ready_now" class="order-input d-inline-block">Как можно скорее</label>
				<input type="radio" class="input bd order-input order-radio js-order-time-sel" value="after" name="ready" id="ready_after">
				<label for="ready_after" class="order-input d-inline-block">К указанному времени</label>
			</div>
			<div class="d-inline-block w-270px"></div>
			<div class="d-inline-block w-100 d-flex justify-content-end">
				<div class="select-container">
					<select>
						<option>Сегодня</option>
						<option>Завтра</option>
						<option>Послезавтра</option>
					</select>
				</div>
				<div class="select-container">
					<select>
						<option>10:00 - 11:00</option>
						<option>12:00 - 13:00</option>
						<option>14:00 - 15:00</option>
					</select>
				</div>
			</div>
			<div class="d-inline-block w-270px">Комментарий к заказу</div>
			<div class="d-inline-flex w-420px">
				<textarea type="text" name="comment_fake" class="input bd w-100 order-input" placeholder="Не более 300 символов"></textarea>
				<input type="hidden" name="comment">
			</div>
		</article>
		{if $deliveries}
		<article class="prod bd w-100 prod-order-panel h-unset">
			<div class="tabs bd-btm mg-btm-20">
				<ul data-tabs class="tabs bd-btm">
					<li class="active"><a data-tab href="cart/#delivery" class="active"><h4 class="g-text">Доставка</h4></a></li>
					<li><a data-tab href="cart/#pickup"><h4 class="g-text">Самовывоз</h4></a></li>
				</ul>
				
				<div data-tabs-content>
					<div data-tabs-pane class="tabs-pane active" id="delivery">
						<div class="d-flex flex-wrap bd-btm">
							{foreach $deliveries as $delivery}
								<article class="prod bd mg-btm prod-dual prod-delivery prod-md d-flex justify-content-between w-344px">
									<div>
										<input type="radio" class="input bd order-input order-radio js-deliv-input" value="{$delivery->id}" name="delivery_id" id="deliveries_{$delivery->id}" {if $delivery_id==$delivery->id}checked{elseif $delivery@first}checked{/if}>
										<label for="deliveries_{$delivery->id}" class="order-input d-block p-relative w-100 h-100">
											<div><b>{$delivery->name}</b></div>
											<div class="prod-main">
												<p class="g-text mg-unset js-deliv-min-from" data-min="{$delivery->free_from}">
													{if $delivery->free_from > 0}Бесплатно от {$delivery->free_from|convert} р.
													{/if}
												</p>
											</div>
											<span class="price-text prod-price js-deliv-price" data-price="{$delivery->price}">
												{if $delivery->price>0}
													{$delivery->price|convert}&nbsp;{$currency->sign}
												{else}
													Бесплатно
												{/if}
											</span>
										</label>
									</div>
								</article>
							{/foreach}
						</div>
						<article class="bd-btm pd-btm-20 pd-top-20">
							<h4 class="mg-unset pd-btm-20">Адрес доставки</h4>
							<input type="text" name="address_fake" class="input bd w-442px order-input mg-right-20px" placeholder="Улица*" data-format=".+" data-notice="Введите улицу">
							<input type="text" class="input bd w-174px order-input js-in-home" placeholder="Дом*" data-format=".+" data-notice="Введите номер дома">
							<input type="text" class="input bd w-247px order-input mg-right-20px js-in-room" placeholder="№ Квартиры или офиса*" data-format=".+" data-notice="Введите номер дома">
							<input type="text" class="input bd w-174px order-input mg-right-20px js-in-pod" placeholder="Подъезд">
							<input type="text" class="input bd w-174px order-input js-in-et" placeholder="Этаж">
							<input type="text" class="input bd w-633px order-input js-in-name" placeholder="Название фирмы или отдела (если есть)">
							<input type="hidden" name="address">
						</article>
					</div>
					<div data-tabs-pane class="tabs-pane" id="pickup">
						<article>
							<div class="g-text">
								г. Березники
							</div>
							<div class="g-adress pd-btm-20">
								пр. Ленина, 70
							</div>
							{literal}
							<script>
								var destinations = {
									'point': [59.421012, 56.799838],
									'center': [59.421242, 56.798312],
								};
								if (1200 > window.innerWidth) {
									destinations['center'] = [59.421242, 56.798312];
								}
                                if (480 > window.innerWidth) {
                                    destinations['center'] = [59.421012, 56.799838];
                                }
							</script>
							{/literal}
							<div id="map" class="pd-btm-20">
								<div id="map_container"></div>
							</div>
						</article>
					</div>
				</div>
			</div>
			<div class="mg-24px-top prod-delivery-accept">
				Отправляя форму, вы соглашаетесь на обработку персональных данных.<br>
				Поля, отмеченные звездочкой (*) обязательны к заполнению.
			</div>
		</article>
		{/if}
	</arcitcle>
{* STATIC >> *} {if !$dyn} {* << STATIC *}
	{if $cart->purchases}
	<arcitcle class="inner-container bd-btm pd-btm-20 js-static-btns">
		{if $cart->auto_coupons}
		<div class="bd-btm pd-btm-20 mg-btm-20">
			<div class="promo-block bd">
				<div class="promo-container g-text d-flex justify-content-between">
					{foreach $cart->auto_coupons as $coupon}
					<div class="promo-container-block">
						<div class="promo-disc">
							-<span>{$coupon->value|intval}</span>%
						</div>
						<div>
							При заказе от <span class="promo-min-price">{$coupon->min_order_price|intval}</span> р.
						</div>
						<div class="promo-need">
							Нужно еще <span>270</span> р.
						</div>
					</div>
					{/foreach}
				</div>
				<div id="promo-progbar"></div>
			</div>
		</div>
		{/if}
		<div class="row full-price">
			<div class="col-sm {if $cart->coupon_discount > 0}d-none{else}d-flex{/if} cart-active-coupon">
				<a href="#" class="btn btn-white btn-promo">У меня есть промокод</a>
				<input type="text" name="coupon_code" class="input bd mg-unset d-none input-promo" placeholder="Введите промокод">
				<input type="submit" name="apply_coupon" class="btn d-none input-promo" value="Применить" onClick="document.cart.submit()">
			</div>
			<div class="col cart-full-price justify-content-center {if $cart->coupon_discount == 0}d-none{else}d-flex{/if}">
				<div>
					<h5>
						Скидка: <span><span id="discount_value" data-value="{$cart->coupon->value}" data-type="{$cart->coupon->type}" data-min="{$cart->coupon->min_order_price}">
						{$cart->coupon_discount|convert}</span>&nbsp;{$currency->sign}</span>
					</h5>
				</div>
			</div>
			<div class="col d-flex cart-full-price justify-content-center">
				<div>
					<h5>Сумма заказа: <span>
						<span 
							id="total_price" 
							data-price="{$cart->total_price}"
							data-price-without-auto-coupon="{$cart->total_price_without_auto_coupon}"
						>
							{$cart->total_price|convert}
						</span>
						&nbsp;{$currency->sign}
					</span></h5>
				</div>
			</div>
		</div>
	</arcitcle>
	<arcitcle class="inner-container pd-btm-20 js-static-btns">
		<div class="row full-price">
			<div class="col-sm d-flex">
				<a href="/" class="btn btn-white-black bd btn-order btn-cart-back" data-step="1">Вернуться в меню</a>
			</div>
			<div class="col-sm d-flex justify-content-end">
				<input type="submit" name="checkout" class="btn btn-order btn-cart-next" value="Оформить заказ" data-step="1">
			</div>
		</div>
	</arcitcle>
	{/if}
</form>
{* /STATIC << *} {/if} {* >> /STATIC *}