<!DOCTYPE html>
{*
	Общий вид страницы
	Этот шаблон отвечает за общий вид страниц без центрального блока.
*}
<html>
<head>
    <base href="{$config->root_url}/"/>
    <title>{if $meta_title}{$meta_title|escape} |{elseif $category->name}{$category->name} |{/if}{if $brand->name}{$brand->name} |{/if} {$settings->site_name|escape}</title>

    {* Метатеги *}
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="description" content="{$meta_description|escape}"/>
    <meta name="keywords" content="{$meta_keywords|escape}"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    {* Канонический адрес страницы *}
    {if isset($canonical)}
        <link rel="canonical" href="{$config->root_url}{$canonical}"/>{/if}

    {* Стили *}
    <link href="design/{$settings->theme|escape}/libs/flickity-slider/flickity.css" rel="stylesheet" type="text/css"
          media="screen"/>
    <link href="design/{$settings->theme|escape}/libs/easy_modal/easy_modal.css" rel="stylesheet" type="text/css"
          media="screen"/>
    <link href="design/{$settings->theme|escape}/libs/grid/bootstrap-grid.css" rel="stylesheet" type="text/css"
          media="screen"/>
    <link href="design/{$settings->theme|escape}/libs/spinner/wan-spinner.css" rel="stylesheet" type="text/css"
          media="screen"/>
    <link href="design/{$settings->theme|escape}/libs/tabby/tabby.css" rel="stylesheet" type="text/css" media="screen"/>
    <link href="design/{$settings->theme|escape}/fonts/fonts.css" rel="stylesheet" type="text/css" media="screen"/>

    <link href="design/{$settings->theme|escape}/css/style.css" rel="stylesheet" type="text/css" media="screen"/>
    <link href="design/{$settings->theme|escape}/css/mobile.css" rel="stylesheet" type="text/css" media="screen"/>
    <link href="design/{$settings->theme|escape}/images/favicon.ico" rel="icon" type="image/x-icon"/>
    <link href="design/{$settings->theme|escape}/images/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
    <link href="design/{$settings->theme|escape}/fonts/fonts.css" rel="stylesheet" type="text/css" media="screen"/>

    {* JQuery *}
    <script src="js/jquery/jquery.js" type="text/javascript"></script>
    <script src="design/{$settings->theme|escape}/libs/flickity-slider/flickity.pkgd.js"
            type="text/javascript"></script>
    <script src="design/{$settings->theme|escape}/libs/easy_modal/easy_modal.js" type="text/javascript"></script>
    <script src="design/{$settings->theme|escape}/libs/spinner/wan-spinner.js" type="text/javascript"></script>
    <script src="design/{$settings->theme|escape}/libs/tabby/tabby.js" type="text/javascript"></script>
    <script src="design/{$settings->theme|escape}/libs/inputmask/jquery.input.mask.min.js"
            type="text/javascript"></script>
    <script src="design/{$settings->theme|escape}/libs/progressbar/progressbar.js" type="text/javascript"></script>
    <script src="design/{$settings->theme|escape}/libs/easydropdown/easydropdown.js" type="text/javascript"></script>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script src="design/{$settings->theme|escape}/js/map.js" type="text/javascript"></script>

    {* Всплывающие подсказки для администратора *}
    {if $smarty.session.admin == 'admin'}
        <script src="js/admintooltip/admintooltip.js" type="text/javascript"></script>
        <link href="js/admintooltip/css/admintooltip.css" rel="stylesheet" type="text/css"/>
    {/if}

    {* Ctrl-навигация на соседние товары *}
    <script type="text/javascript" src="js/ctrlnavigate.js"></script>

    {* js-проверка форм *}
    <script src="js/baloon/js/baloon.js" type="text/javascript"></script>
    <link href="js/baloon/css/baloon.css" rel="stylesheet" type="text/css"/>

    {* Автозаполнитель поиска *}
    {literal}
        <script src="js/autocomplete/jquery.autocomplete-min.js" type="text/javascript"></script>
        <style>
            .autocomplete-suggestions {
                background-color: #ffffff;
                width: 100px;
                overflow: hidden;
                border: 1px solid #e0e0e0;
                padding: 5px;
            }

            .autocomplete-suggestions .autocomplete-suggestion {
                cursor: default;
            }

            .autocomplete-suggestions .selected {
                background: #F0F0F0;
            }

            .autocomplete-suggestions div {
                padding: 2px 5px;
                white-space: nowrap;
            }

            .autocomplete-suggestions strong {
                font-weight: normal;
                color: #3399FF;
            }
        </style>
        <script>
            $(function () {
                //  Автозаполнитель поиска
                $(".input_search").autocomplete({
                    serviceUrl: 'ajax/search_products.php',
                    minChars: 1,
                    noCache: false,
                    onSelect:
                        function (suggestion) {
                            $(".input_search").closest('form').submit();
                        },
                    formatResult:
                        function (suggestion, currentValue) {
                            var reEscape = new RegExp('(\\' + ['/', '.', '*', '+', '?', '|', '(', ')', '[', ']', '{', '}', '\\'].join('|\\') + ')', 'g');
                            var pattern = '(' + currentValue.replace(reEscape, '\\$1') + ')';
                            return (suggestion.data.image ? "<img align=absmiddle src='" + suggestion.data.image + "'> " : '') + suggestion.value.replace(new RegExp(pattern, 'gi'), '<strong>$1<\/strong>');
                        }
                });
            });
        </script>
    {/literal}


</head>
<body>

<header class="container-fluid">
    <section class="container d-flex mg-14px-vert justify-content-end">
        <a class="fl-left d-flex mobile-menu mobile-menu__open" hidden>
            <img src="design/{$settings->theme|escape}/img/menu-icon.png"
                 srcset="design/{$settings->theme|escape}/img/menu-icon@2x.png 2x,
             design/{$settings->theme|escape}/img/menu-icon@3x.png 3x"
                 class="menu-icon">
        </a>
        <a href="/" class="d-flex header-item-first">
            <div class="fl-left d-flex logo">
                <img src="design/{$settings->theme|escape}/img/logo.png">
            </div>
            <div class="d-flex flex-column justify-content-center">
                <div class="g-text">С НАМИ</div>
                <div class="logo-text">ВКУСНЕЕ</div>
            </div>
        </a>
        <a href="cart/" class="fl-right d-flex mobile-menu mobile-menu__cart" hidden>
            <img src="design/{$settings->theme|escape}/img/online-shopping-cart.png"
                 srcset="design/{$settings->theme|escape}/img/online-shopping-cart@2x.png 2x,
             design/{$settings->theme|escape}/img/online-shopping-cart@3x.png 3x"
                 class="online-shopping-cart">
        </a>
        <div class="d-flex header-item">
            <div class="fl-left d-flex header-item-icon">
                <img src="design/{$settings->theme|escape}/img/i-phone.svg">
            </div>
            <div class="d-flex flex-column justify-content-center">
                <div class="g-text">Номер для заказа</div>
                <h5><a href="tel:{$settings->site_phone}">{$settings->site_phone}</a></h5>
            </div>
        </div>
        <div class="d-flex header-item">
            <div class="fl-left d-flex header-item-icon">
                <img src="design/{$settings->theme|escape}/img/i-time.svg">
            </div>
            <div class="d-flex flex-column justify-content-center">
                <div class="g-text">Заказы принимаются</div>
                <h5>{$settings->sets_timework}</h5>
            </div>
        </div>
        <div class="d-flex header-item-cart" id="cart_informer">
            {include file='cart_informer.tpl'}
        </div>
        <div class="d-flex header-item-cart-btn">
            <div class="d-flex flex-column justify-content-center">
                <a href="cart/" class="btn btn-header">Корзина</a>
            </div>
        </div>
        <div class="d-flex">
            <div class="d-flex flex-column justify-content-center p-relative">
                <!-- Добавить класс authorized, чтобы появлялось всплывающее окошко -->
                <a href="#" {if !$user}id="login_link"{/if}
                   class="btn btn-header btn-user prod-user-meta {if $user}authorized{/if}">
                    <img class="prod-user-meta" src="design/{$settings->theme|escape}/img/i-user.svg">
                </a>
                {if $user}
                    {if $user->name == $user->phone} {$uname = 'Ваше имя'} {else} {$uname = $user->name} {/if}
                    <div class="prod prod-user bd w-270px h-unset prod-user-meta">
                        <div class="prod-user-meta">
                            <h3 class="prod-user-meta">{$uname|escape}
                                <span class="fl-right prod-user-close">
                                    <img class="prod-user-meta" src="design/{$settings->theme|escape}/img/x.svg">
                                </span>
                            </h3>
                        </div>
                        <div class="prod-user-meta">
                            <a href="/user/#private" class="g-text prod-user-meta">Личный кабинет</a>
                        </div>
                        <div class="prod-user-meta">
                            <a href="/user/#orders" class="g-text prod-user-meta">История заказов</a>
                        </div>
                        <div class="prod-user-meta">
                            <a href="user/logout" class="g-text prod-user-meta">Выход</a>
                        </div>
                    </div>
                {/if}
            </div>
        </div>
    </section>
    {if $canonical == ""}
        <section class="container-fluid main-menu align-content-center">
            <nav class="container h-100 d-flex align-content-center">
                <ul>
                    <li><a href="#"><span><img src="design/{$settings->theme|escape}/img/i-tag.svg"></span>Акции</a></li>
                    <li><a href="#main_kits" class="js-scroll-to">Наборы</a></li>
                    <li><a href="#main_firmrolls" class="js-scroll-to">Фирменные роллы</a></li>
                    <li><a href="#main_zaprolls" class="js-scroll-to">Запеченные роллы</a></li>
                    <li><a href="#main_spicyrolls" class="js-scroll-to">Острые роллы</a></li>
                    <li><a href="#main_smallrolls" class="js-scroll-to">Малые роллы</a></li>
                    <li><a href="#main_drinks" class="js-scroll-to">Напитки</a></li>
                    <li><a href="#main_adds" class="js-scroll-to">Дополнительно</a></li>
                    {foreach $pages as $p}
                        {if ($p->menu_id == 1 || $p->menu_id == 2) && $p->visible == 1 && $p->id != 1}
                            <li><a href="{$p->url}" data-page="{$p->id}">{$p->name|escape}</a></li>
                            {break}
                        {/if}
                    {/foreach}
                    <li class="sub-menu">
                        <a href="#">Еще</a>
                        <ul>
                            {$cnt = 0}
                            {foreach $pages as $p}
                                {if ($p->menu_id == 1 || $p->menu_id == 2) && $p->visible == 1 && $p->id != 1 && $cnt++}
                                    <li><a href="{$p->url}" data-page="{$p->id}">{$p->name|escape}</a></li>
                                {/if}
                            {/foreach}
                        </ul>
                    </li>
                </ul>
            </nav>
        </section>
    {/if}
</header>
<main>
    <!-- content -->
    {$content}
    <!-- content end -->
</main>
<footer class="container-fluid">
    <section class="container">
        <div class="row pd-btm-20 pd-top-20 ">
            <div class="col-12 col-sm-4 d-flex">
                <div class="footer-logo">
                    <img src="design/{$settings->theme|escape}/img/logo2.svg">
                </div>
                <div>
                    <h5>«{$settings->site_name}»</h5>
                    <div>
                        Доставка роллов Березники<br>{$settings->company_hint}
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-5 foot-menu">
                <ul>
                    <li><a href="#">Акции</a></li>
                    <li><a href="#main_kits" class="js-scroll-to">Наборы</a></li>
                    <li><a href="#main_firmrolls" class="js-scroll-to">Фирменные роллы</a></li>
                    <li><a href="#main_zaprolls" class="js-scroll-to">Запеченные роллы</a></li>
                    <li><a href="#main_spicyrolls" class="js-scroll-to">Острые роллы</a></li>
                    <li><a href="#main_smallrolls" class="js-scroll-to">Малые роллы</a></li>
                    <li><a href="#main_drinks" class="js-scroll-to">Напитки</a></li>
                    <li><a href="#main_adds" class="js-scroll-to">Дополнительно</a></li>
                    {foreach $pages as $p}
                        {if $p->menu_id == 1 && $p->visible == 1 && $p->id != 1}
                            <li><a href="{$p->url}" data-page="{$p->id}">{$p->name|escape}</a></li>
                        {/if}
                    {/foreach}
                </ul>
            </div>
            <div class="col-12 col-sm-3 col-phohe">
                <div class="footer-phone fl-right">{$settings->site_phone}</div>
                <div class="fl-right">Принимаем заказы {$settings->sets_timework}</div>
                <div class="fl-right">
                    <a href="{$settings->sets_vk}" target="_blank" class="icon-soc vk"><img
                                src="design/{$settings->theme|escape}/img/vk.svg"></a>
                    <a href="{$settings->sets_ins}" target="_blank" class="icon-soc ins"><img
                                src="design/{$settings->theme|escape}/img/ins.svg"></a>
                </div>
            </div>
        </div>
        <div class="devider mg-btm-20"></div>
        <div class="row pd-btm-20">
            <div class="col">
                <div>{$settings->sets_copyright}</div>
            </div>
            <div class="col foot-hor-menu">
                <ul>
                    {foreach $pages as $p}
                        {if $p->menu_id == 2 && $p->visible == 1 && $p->id != 1}
                            <li><a href="{$p->url}" data-page="{$p->id}">{$p->name|escape}</a></li>
                        {/if}
                    {/foreach}
                </ul>
            </div>
            <div class="col">
                <div class="creator"><a href="#">Разработка сайта PROSTOR design</a></div>
            </div>
        </div>
    </section>
</footer>

<!-- Modals -->
<section>
    <article id="login_modal" class="easy_modal">
        <span class="easy_modal_close"><img src="design/{$settings->theme|escape}/img/x.svg"></span>
        <h2>Вход на сайт</h2>
        <p class="g-text">Ведите историю своих покупок и получайте персональные скидки.</p>
        <div class="requestInform error d-none"></div>
        <input type="text" name="phone" class="input bd w-100" placeholder="Ваш номер телефона"
               placeholder="+7___-__-__" data-format=".+" data-notice="Введите телефон">
        <div>
            <a href="#" id="register_phone_check" class="btn fl-right w-150px">Отправить код</a>
        </div>
    </article>

    <article id="login_modal_sms" class="easy_modal">
        <span class="easy_modal_close"><img src="design/{$settings->theme|escape}/img/x.svg"></span>
        <h2>Вход на сайт</h2>
        <p class="g-text">Ведите историю своих покупок и получайте персональные скидки.</p>
        <div class="requestInform error d-none"></div>
        <input type="text" name="code" class="input bd w-100" placeholder="Код из СМС">
        <div>
            <a href="#" id="timeout_sms" class="input bd mg-unset input-btn-white fl-left">Отправить снова</a>
            <a href="#" id="register_code_check" class="btn fl-right w-150px">Войти</a>
        </div>
    </article>
</section>
<!-- <a href="#easy_modal_1" class="easy_modal_open">Open modal</a> -->
<div id="easy_modal_overlay"></div>
<section>

    <article id="sidenav" class="sidenav  flex-column">
        <div class=" sidenav_userinfo align-self-start">
            <div class="d-flex flex-column justify-content-center p-relative">
                <!-- Добавить класс authorized, чтобы появлялось всплывающее окошко -->
                <a href="#" {if !$user}id="login_link_side"{/if}
                   class="btn btn-header btn-user prod-user-meta {*if $user}authorized{/if*}">
                    <img class="prod-user-meta" src="design/{$settings->theme|escape}/img/i-user.svg">
                    {if $user}
                    {if $user->name == $user->phone} {$uname = 'Ваше имя'} {else} {$uname = $user->name} {/if}
                        <span>{$uname|escape}</span>

                    {else}
                        <span>Вход</span>
                    {/if}
                </a>

                {if $user}
                    {if $user->name == $user->phone} {$uname = 'Ваше имя'} {else} {$uname = $user->name} {/if}
                    <div class="sidebar-user__links bd-btm">
                        <div class="sidebar-user__link">
                            <a href="/user/#private" class="g-text prod-user-meta">Личный кабинет</a>
                        </div>
                        <div class="sidebar-user__link">
                            <a href="/user/#orders" class="g-text prod-user-meta">История заказов</a>
                        </div>
                        <div class="sidebar-user__link">
                            <a href="user/logout" class="g-text prod-user-meta">Выход</a>
                        </div>
                    </div>


                    <div class="prod prod-user bd w-270px h-unset prod-user-meta">
                        <div class="prod-user-meta">
                            <h3 class="prod-user-meta">{$uname|escape}
                                <span class="fl-right prod-user-close">
                                    <img class="prod-user-meta" src="design/{$settings->theme|escape}/img/x.svg">
                                </span>
                            </h3>
                        </div>
                        <div class="prod-user-meta">
                            <a href="/user/#private" class="g-text prod-user-meta">Личный кабинет</a>
                        </div>
                        <div class="prod-user-meta">
                            <a href="/user/#orders" class="g-text prod-user-meta">История заказов</a>
                        </div>
                        <div class="prod-user-meta">
                            <a href="user/logout" class="g-text prod-user-meta">Выход</a>
                        </div>
                    </div>
                {/if}
            </div>
        </div>

        <div class="sidenav_links">
            <ul>
                <li><a href="/">Меню</a></li>
                <li><a href="cart/">Корзина</a></li>
                {$cnt = 0}
                {foreach $pages as $p}
                    {if ($p->menu_id == 1 || $p->menu_id == 2) && $p->visible == 1 && $p->id != 1 && $cnt++}
                        <li><a href="{$p->url}" data-page="{$p->id}">{$p->name|escape}</a></li>
                    {/if}
                {/foreach}
            </ul>
        </div>

        <div class="sidenav_info align-self-end ">
            <div class="d-flex header-item ">
                <div class="fl-left d-flex header-item-icon">
                    <img src="design/{$settings->theme|escape}/img/i-phone.svg">
                </div>
                <div class="d-flex flex-column justify-content-center">
                    <div class="g-text">Прием заказов</div>
                    <h5><a href="tel:{$settings->site_phone}">{$settings->site_phone}</a></h5>
                </div>
            </div>
            <div class="d-flex header-item ">
                <div class="fl-left d-flex header-item-icon">
                    <img src="design/{$settings->theme|escape}/img/i-time.svg">
                </div>
                <div class="d-flex flex-column justify-content-center">
                    <div class="g-text">Время работы</div>
                    <h5>{$settings->sets_timework}</h5>
                </div>
            </div>
        </div>

    </article>
    <div class="sidenav sidenav_overlay"></div>
</section>


<script src="design/{$settings->theme|escape}/js/app.js" type="text/javascript"></script>
<script src="design/{$settings->theme|escape}/js/mobile.js" type="text/javascript"></script>

</body>
</html>