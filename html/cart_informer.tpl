{* Информера корзины (отдаётся аяксом) *}
<div class="d-flex flex-column justify-content-center" {if $cart->total_products == 0}hidden{/if}>
	<h5>{$cart->total_price|convert} {$currency->sign|escape}</h5>
	<div class="g-text">{$cart->total_products} {$cart->total_products|plural:'товар':'товаров':'товара'}</div>
</div>
