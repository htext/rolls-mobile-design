{* Главная страница магазина *}

{* Для того чтобы обернуть центральный блок в шаблон, отличный от index.tpl *}
{* Укажите нужный шаблон строкой ниже. Это работает и для других модулей *}
{$wrapper = 'index.tpl' scope=parent}

{* Канонический адрес страницы *}
{$canonical="" scope=parent}




{get_slides var=slides visible=1 limit=20}
{if $slides|count > 0}
<section class="section-slider">
	<article class="main-carousel home-carousel">
		{foreach $slides as $s}
		<div class="carousel-cell">
			<img src="{$s->image}">
			<div class="slider-content container">
				<div class="slider-h1">{$s->name|escape}</div>
				<div class="slider-h2">{$s->hint|escape}</div>
				<div class="slider-h3">{$s->description|escape}</div>
			</div>
		</div>
		{/foreach}
	</article>
</section>
{/if}
{* {$categories|print_r} *}
{$kits = $api->products->get_fit_products(['visible' => 1, 'category_id' => 26])}
{if $kits|count > 0}
<section class="container">
	<h2 id="main_kits">Наборы</h2>
	<div class="row">
	{foreach $kits as $product}
		{include file="products_item.tpl" product=$product type='kit'}
	{/foreach}
	</div>
</section>
{/if}
{$firmrolls = $api->products->get_fit_products(['visible' => 1, 'category_id' => 27])}
{if $firmrolls|count > 0}
<section class="container">
	<h2 id="main_firmrolls">Фирменные роллы</h2>
	<div class="row">
	{foreach $firmrolls as $product}
		{include file="products_item.tpl" product=$product type='roll'}
	{/foreach}
	</div>
</section>
{/if}
{$firmrolls = $api->products->get_fit_products(['visible' => 1, 'category_id' => 28])}
{if $firmrolls|count > 0}
<section class="container">
	<h2 id="main_zaprolls">Запеченные роллы</h2>
	<div class="row">
	{foreach $firmrolls as $product}
		{include file="products_item.tpl" product=$product type='roll'}
	{/foreach}
	</div>
</section>
{/if}
{$spicyrolls = $api->products->get_fit_products(['visible' => 1, 'category_id' => 29])}
{if $spicyrolls|count > 0}
<section class="container">
	<h2 id="main_spicyrolls">Острые роллы</h2>
	<div class="row">
	{foreach $spicyrolls as $product}
		{include file="products_item.tpl" product=$product type='roll' spicy=1}
	{/foreach}
	</div>
</section>
{/if}
{$firmrolls = $api->products->get_fit_products(['visible' => 1, 'category_id' => 30])}
{if $firmrolls|count > 0}
<section class="container">
	<h2 id="main_smallrolls">Малые роллы</h2>
	<div class="row">
	{foreach $firmrolls as $product}
		{include file="products_item.tpl" product=$product type='roll'}
	{/foreach}
	</div>
</section>
{/if}
{$drinks = $api->products->get_fit_products(['visible' => 1, 'category_id' => 31])}
{if $drinks|count > 0}
<section class="container">
	<h2 id="main_drinks">Напитки</h2>
	<div class="row">
	{foreach $drinks as $product}
		{include file="products_item.tpl" product=$product type='drink'}
	{/foreach}
	</div>
</section>
{/if}
{$adds = $api->products->get_fit_products(['visible' => 1, 'category_id' => 32])}
{if $adds|count > 0}
<section class="container">
	<h2 id="main_adds">Дополнительно</h2>
	<div class="row">
	{foreach $adds as $product}
		{include file="products_item.tpl" product=$product type='adds'}
	{/foreach}
	</div>
</section>
{/if}
<section class="container section-delivery">
	<h2>Доставка и оплата</h2>
	<div class="row">
		<div class="col-12 col-lg-12 col-xl-6">
			<article class="p-text-block">
				<h4>Как оплатить?</h4>
				<p class="p-text">Принимаем к оплате <b>только наличные</b>, терминал отсутствует.</p>
			</article>
			<article class="p-text-block">
				<h4>Подтверждение заказа</h4>
				<p class="p-text">Заказ, оформленный на сайте, <b>подтверждается после устного согласия клиента</b>. Если Вам не перезвонил оператор в течение 10 минут после оформления заказа, рекомендуем оформить его повторно или связаться с нами по телефону <a class="a-link no-warp" href="#">8 992 216-1-216</a>.</p>
			</article>
		</div>
		<div class="col-12 col-lg-12 col-xl-6">
			<article class="p-text-block">
				<h4>Как оплатить?</h4>
				<p class="p-text">Принимаем к оплате <b>только наличные</b>, терминал отсутствует.</p>
			</article>
			<article class="p-text-block">
				<h4>Как оплатить?</h4>
				<p class="p-text">Принимаем к оплате <b>только наличные</b>, терминал отсутствует.</p>
			</article>
		</div>
	</div>
</section>
<section class="container page section-delivery__uncity">
	<div class="gray-block">
		<h4>ДОСТАВКА ВНЕ ГОРОДА</h4>
		<ul>
			<li><b>Бесплатная доставка до Азот, Ависма, Сода.</b></li>
			<li><b>Бесплатная доставка до Усолья</b> при заказе <b>от 2000 рублей, если менее</b>, то доставка составит <b>280 рублей</b> к заказу.</li>
			<li>Бесплатная доставка до Зырянки, Суханово, Пермяково, Новожилово при заказе от 1200 рублей, если менее, то доставка составит 70 рублей к заказу.</li>
			<li>Бесплатная доставка до БКПРУ-2, БКПРУ-3, БКПРУ-4 при заказе от 1800 рублей, если менее, то доставка составит 140 рублей к заказу.</li>
		</ul>
	</div>
</section>
<section class="container-fluid mg-top-60px section-map">
	<!-- MAP START -->
	<div class="large-12 columns">
		{literal}
		<script>
			var destinations = {
				'point': [59.421012, 56.799838],
				'center': [59.421242, 56.798312],
			};
			if (1200 > window.innerWidth) {
				destinations['center'] = [59.421242, 56.798312];
			}
			if (480 > window.innerWidth) {
				destinations['center'] = [59.421012, 56.799838];
			}
		</script>
		{/literal}
		<div id="map">
			<div id="map_container"></div>
			<section class="container p-relative">
				<div class="map-window d-flex flex-column justify-content-between">
					<div class="bd-btm orange-color pd-btm-20">
						<div><span><img src="design/{$settings->theme|escape}/img/i-point-min.svg"></span>г. Березники</div>
						<h1>пр. Ленина, 70</h1>
					</div>
					<div>
						<div class="g-text"><span><img src="design/{$settings->theme|escape}/img/i-phone-min.svg"></span>Телефон для заказов</div>
						<h1>{$settings->site_phone}</h1>
					</div>
				</div>
			</section>
		</div>
	</div>
	<!-- MAP END -->
</section>