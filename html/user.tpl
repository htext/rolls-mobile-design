{* Шаблон страницы зарегистрированного пользователя *}
<section class="container d-flex justify-content-center">
	<arcitcle class="inner-container">
		<h1>Личный кабинет</h1>
		<div class="tabs">
			<ul data-tabs class="tabs bd-btm">
				<li class="active"><a data-tab href="user/#private" class="active"><h4 class="g-text">Личные данные</h4></a></li>
				<li><a data-tab href="user/#orders"><h4 class="g-text">История заказов</h4></a></li>
			</ul>
			
			<div data-tabs-content>
				<div data-tabs-pane class="tabs-pane active" id="private">
					{if $error}
					<div class="requestInform error">
						{if $error == 'empty_name'}Введите имя
						{elseif $error == 'empty_email'}Введите email
						{elseif $error == 'empty_password'}Введите пароль
						{elseif $error == 'user_exists'}Пользователь с таким email уже зарегистрирован
						{else}{$error}{/if}
					</div>
					{/if}
					<div class="bd-btm pd-btm-20 mg-btm-20">
						<div class="promo-block bd">
							<div class="promo-container g-text d-flex justify-content-between">
								<div class="promo-container-block">
									<div class="promo-disc">
										-2.5%
									</div>
									<div>
										При заказе от 1500 р.
									</div>
									<div class="promo-need active">
										Осталось 28 дней <span class="i-inf"><img src="design/{$settings->theme|escape}/img/i-inf.svg"></span>
									</div>
								</div>
								<div class="promo-container-block">
									<div class="promo-disc">
										-4%
									</div>
									<div>
										При заказе от 2000 р.
									</div>
									<div class="promo-need">
										Нужно еще 770 р.
									</div>
								</div>
								<div class="promo-container-block">
									<div class="promo-disc">
										-6%
									</div>
									<div>
										При заказе от 1500 р.
									</div>
									<div class="promo-need">
										Нужно еще 270 р.
									</div>
								</div>
							</div>
							<div id="promo-progbar"></div>
						</div>
					</div>
					<form method="POST">
						<div class="bd-btm">
							{if $user->name == $user->phone} {$uname = 'Ваше имя'} {else} {$uname = $user->name} {/if}
							<article class="w-100 d-flex justify-content-between">
								<div class="d-flex mg-auto-vert">Ваше имя</div>
								<input type="text" name="name" class="input bd w-464px" value="{$uname|escape}">
							</article>
							<article class="w-100 d-flex justify-content-between">
								<div class="d-flex mg-auto-vert">Телефон</div>
								<input type="text" class="input bd w-464px" placeholder="{$user->phone|escape}" disabled>
								<input type="hidden" name="phone" class="input bd w-464px" value="{$user->phone|escape}">
							</article>
							<article class="w-100 d-flex justify-content-between">
								<div class="d-flex mg-auto-vert">E-mail:</div>
								<input type="text" name="email" class="input bd w-464px" value="{$user->email|escape}">
							</article>
							{* <article class="w-100 d-flex justify-content-between mg-btm-20">
								<div class="d-flex mg-auto-vert">День рождения:</div>
								<div class="d-flex justify-content-between">
									<div class="select-container">
										<select>
											<option value="" data-placeholder>День</option>
											<option value="1">1</option>
											<option value="2">2</option>
										</select>
									</div>
									<div class="select-container lk">
										<select>
											<option value="" data-placeholder>Месяц</option>
											<option value="1">Январь</option>
											<option value="2">Февраль</option>
										</select>
									</div>
								</div>
							</article> *}
						</div>
						<div><input type="submit" class="btn fl-right mg-24px-top" value="Сохранить изменения"></div>
					</form>
				</div>
				<div data-tabs-pane class="tabs-pane" id="orders">
					{if $orders}
						{foreach $orders as $order}
						<article class="prod prod-order bd mg-btm w-100 d-flex align-content-center">
							<div>{$order|os_log}
								<h5>Заказ №{$order->id}</h5>
								<div class="g-text">
									{$order->date|date}, 
									{* {if $order->paid == 1}оплачен, {/if}  *}
									{if $order->status == 0}
										ждет обработки
									{elseif $order->status == 1}
										в обработке
									{elseif $order->status == 2}
										<span class="green-text">выполнен</span>
									{/if}
								</div>
							</div>
							<div>
								{$purchases = $api->orders->get_purchases($order->id)}
								<div class="g-text">Товаров</div>
								<div>{$purchases|count} шт.</div>
							</div>
							<div>
								<div class="g-text">Скидка</div>
								<div>{if $order->discount > 0}{$order->discount|convert}&nbsp;{$currency->sign}{else}-{/if}</div>
							</div>
							<div>
								<div class="g-text">Сумма</div>
								<div class="price-text">{$order->total_price|convert}&nbsp;{$currency->sign}</div>
							</div>
							<div>
								<a href="order/{$order->url}" class="btn btn-white">Детали</a>
							</div>
						</article>
						{/foreach}
					{else}
					Заказов нет
					{/if}
				</div>
			</div>
		</div>
	</arcitcle>
</section>