{* Страница заказа *}

{$meta_title = "Ваш заказ №`$order->id`" scope=parent}

<section class="container d-flex justify-content-center flex-column">
	<arcitcle class="inner-container pd-btm-20">
		<h1>Заказ №{$order->id} 
			{if $order->status == 0}принят{/if}
			{if $order->status == 1}в обработке{elseif $order->status == 2}выполнен{/if}
			{if $order->paid == 1}, оплачен{else}{/if}
		</h1>
		{$order|os_log}
		{* <article class="prod bd w-100 prod-order-panel mg-btm-20">
			<div class="bd-btm"><h4>Состояние заказа</h4></div>
			<table class="order-table">
				<tr class="bd-btm">
					<td><div class="d-flex"><span><img src="img/i-check.svg"></span><b>Подтверждение оператором</b></div></td>
					<td class="g-text">5 - 10 мин.</td>
				</tr>
				<tr class="bd-btm">
					<td><div class="d-flex"><span><img src="img/i-load.svg"></span><b>Формирование</b></div></td>
					<td class="g-text">10 - 15 мин.</td>
				</tr>
				<tr class="bd-btm">
					<td><div class="d-flex"><span><img src="img/i-load.svg"></span><b>Доставка</b></div></td>
					<td class="g-text">25 - 35 мин.</td>
				</tr>
				<tr>
					<td><div class="d-flex"><span><img src="img/i-load.svg"></span><b>Заказ выполнен</b></div></td>
					<td class="g-text">Приятного аппетита</td>
				</tr>
			</table>
		</article> *}
		<article class="prod bd w-100 prod-order-panel mg-btm-20">
			<div><h4>Детали заказа</h4></div>
			{$total_amount = 0}
			{$total_full_price = 0}
			<div class="main-carousel cart-additionals mg-btm-20">
				{foreach $purchases as $purchase}
					{$product = $purchase->product}
					{$image = $product->images|first}
					{$options = $api->features->get_product_options($product->id)}
					{$option = ''}
					{$total_amount = $total_amount + $purchase->amount}
					{$total_full_price = $total_full_price + $purchase->variant->price * $purchase->amount}
					{foreach $options as $opt}
						{if $opt->feature_id == 152}
							{$option = $opt->value}
						{elseif $opt->feature_id == 153}
							{$prod_dual = $opt->value}
						{elseif $opt->feature_id == 154}
							{$option = $opt->value}
						{/if}
					{/foreach}
					<div class="carousel-cell">
						<article class="prod bd mg-btm prod-dual prod-cart-adds prod-md d-flex justify-content-between">
							<div>
								<div class="prod-images p-relative">
									<div class="prod-labels">
										{if $product->hit}<span class="prod-label prod-label-hit">HIT</span>{/if}
										{if $product->new}<span class="prod-label prod-label-new">NEW</span>{/if}
									</div>
									<div class="prod-img d-flex">
										<img class="mg-auto" src="{$image->filename|resize:200:200}" alt="{$product->name|escape}">
									</div>
								</div>
							</div>
							<div>
								<h5>{$product->name|escape}</h5>
								<div class="g-text">{$option} / {$purchase->amount} шт.</div>
								<div class="margin"></div>
								<span class="price-text prod-price">{$purchase->variant->price|convert} {$currency->sign}</span>
							</div>
						</article>
					</div>
				{/foreach}
			</div>
			<div class="bd-btm"></div>
			<table class="order-table order-table-2">
				<tr class="bd-btm">
					<td><div class="d-flex"><b>Товары</b></div></td>
					<td class="g-text">{$total_amount} шт.</td>
					<td class="price-text">{$total_full_price|convert} {$currency->sign}</td>
				</tr>
				<tr class="bd-btm">
					<td><div class="d-flex"><b>Способ доставки</b></div></td>
					<td class="g-text">{$delivery->name|escape}</td>
					<td class="price-text">
						{if $order->delivery_price>0}
							{$delivery->price|convert}&nbsp;{$currency->sign}
						{else}
							Бесплатно
						{/if}
					</td>
				</tr>
				<tr class="bd-btm">
					{if $order->auto_coupon_code}
						{$acoup = $api->coupons->get_coupon($order->auto_coupon_code)}
					{/if}
					<td><div class="d-flex"><b>Скидка</b></div></td>
					<td class="g-text">{if $acoup}При заказе от {$acoup->min_order_price} р.{else}нет{/if}</td>
					<td class="price-text">{if $acoup}{$acoup->value}%{else}-{/if}</td>
				</tr>
				<tr {* class="bd-btm" *}>
					<td><div class="d-flex"><b>Промокод</b></div></td>
					<td class="g-text">
						{if $order->coupon_discount > 0}
							{$order->coupon_code|escape}
						{else}
							нет
						{/if}
					</td>
					<td class="price-text">
						{if $order->coupon_discount > 0}
							&minus;{$order->coupon_discount|convert}&nbsp;{$currency->sign}
						{else}
							-
						{/if}
					</td>
				</tr>
				{* <tr>
					<td><div class="d-flex"><b>Скидка клуба</b></div></td>
					<td class="g-text">нет</td>
					<td class="price-text">-</td>
				</tr> *}
				<tr>
					<td><div class="d-flex"><b>Стоимость заказа</b></div></td>
					<td class="g-text"></td>
					<td class="price-text">{$order->total_price|convert} {$currency->sign}</td>
				</tr>
			</table>
		</article>
	</arcitcle>
</section>